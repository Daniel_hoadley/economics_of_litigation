import streamlit as st 
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np


st.sidebar.title("APOLLO")

try:

    # CLAIMANT DATA CAPTURE
    # Value of claim (J)
    # Probability C will win (p_c)
    # Claimant's expected costs if case taken all the way to trial (C_c)
    
    J = st.sidebar.number_input("Value of the claim")
    costs_follow = st.sidebar.checkbox("Costs follow the event", value=True, help="Loser pays winner's costs") 
    p_c = st.sidebar.slider("Probability that judgment will be given for the claimant", min_value=0.0, max_value=1.0,)
    C_c = st.sidebar.number_input("Claimant's expected costs")


    # DEFENDANT DATA CAPTURE
    p_d = 1 - p_c # judgment probability for defendant
    C_d = st.sidebar.number_input("Defendant's expected costs")

    total_costs = C_c + C_d
   
    # PAY-OFF & EXPOSURE
    if costs_follow:
        if p_c > p_d: # C wins
            T_c = np.around(p_c * J, 3) # when costs follow event and C wins, do not deduct C's costs
            T_d = np.around((p_c * J) + C_c + C_d, 3) # D pays C's costs
        else: # D wins
            T_c = np.around(0 - total_costs, 3) # when costs follow event and C loses, deduct C's and D's costs
            T_d = np.around(p_c * J - C_d, 3) # D doesn't pay C's costs

    else:
        T_c = np.around(p_c * J - C_c, 3) # when costs follow event, do not deduct C's costs
        T_d = np.around((p_c * J) + C_d, 3) # D doesn't pay C's costs
    

    NEV = np.round(C_c/J, 2)

    if p_c > (C_c/J):
        st.sidebar.markdown("**Litigate**")
        #litigate = True
    else:
        st.sidebar.markdown("**Don't pursue claim**")
        #litigate = False

    # if all(v is not None for v in [J, p_c, C_c, C_d]):
    st.sidebar.markdown(f"Litigation threshold: {NEV}")
    st.sidebar.markdown(f"Value of claim: £{J}")
    st.sidebar.markdown(f"Probability claimant will win: {p_c}")
    st.sidebar.markdown(f"Claimant's expected costs: £{C_c}")
    
    st.sidebar.markdown("---")
    st.sidebar.markdown(f"Probability defendant will win: {p_d}")
    st.sidebar.markdown(f"Defendant's expected costs: £{C_d}")

    left_column, right_column = st.beta_columns(2)
    left_column.markdown(f"### **Claimant's pay-off:** £{T_c}")
    right_column.markdown(f"### **Defendant's exposure:** £{T_d}")

    px, py = (100, 100)
    p_dg = np.linspace(0, 1, px)
    p_cg = np.linspace(0, 1, py)
    pp_d, pp_c = np.meshgrid(p_dg, p_cg)

    s = np.zeros(shape = pp_c.shape) 

    f, ax = plt.subplots(figsize=(6, 6))

    for i,p_d_ in enumerate(p_dg):
        for j, p_c_ in enumerate(p_cg):
            #print(p_d_, p_c_)
            #print(i,j)
            
            T_c = p_c_*J-C_c  # Claimant payoff
            T_d = p_d_*J+C_d  # Defendant exposure


            if T_c > T_d: # Litigate
                s[i,j] = 1 
            elif T_c < 0: # Negative Expected Value
                s[i,j] = -1
            else: # Settelment 
                s[i,j] = 0
        
    contours = plt.contourf(p_dg, p_cg, s.T, 2, cmap='hsv')


    plt.axis('scaled')
    plt.xlabel("$p_d$", fontsize=25)
    plt.ylabel("$p_c$", fontsize=25)

    ax.plot([0, 1], [0, 1], ls="--", c="0")

    ax.plot([0, 1], [NEV, NEV], ls="--", c="0")
    plt.plot(p_d, p_c, marker=(3, 0, 90), color="black", ls="dotted", markersize=12)
    plt.text(1.05, NEV-0.02, "$p_c^*= {nev:.4f}$".format(nev=NEV), fontsize=12)

    # Hard Coded 
    plt.text(0.2, 0.8, "$T_C > T_D$".format(nev=NEV), fontsize=20, c='1') # Litigate region
    plt.text(0.5, 0.4, "$T_C < S < T_D$".format(nev=NEV), fontsize=20, c='1') # Settlment region
    plt.text(0.5, NEV/2-0.02, "$T_C < 0$".format(nev=NEV), fontsize=20, c='1') # NEV region 


    #plt.colorbar()
    st.pyplot(plt)

except ZeroDivisionError or ValueError:
    st.info("Enter value of claim to begin analysis")
