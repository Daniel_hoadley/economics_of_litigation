# ECONOMICS OF LITIGATION #

Basic Streamlit implementation of elements of the *Economics of Litigiation* paper. 

## Setup and Usage

1. Install dependencies

```pip install -r requirements.txt```


2. Run the Streamlit application

```streamlit run app.py```

By default the app will launch on localhost listening on port 8501. 